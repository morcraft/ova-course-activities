const _ = require('lodash')

module.exports = {
    checkEnvironment: function(){
        if (!_.isObject(location)) {
            throw new Error('Environment seems to be wrong. Couldn\'t find location.')
        }
    },
    isLocal: function () {
        this.checkEnvironment();
        return location.href.indexOf('http://localhost') > -1 || location.href.indexOf('file://') > -1;
    },
    default: 'http://moodle.bitpointer.co',
    getBasePath: function(){
        this.checkEnvironment();
        if(location.origin.indexOf('://esapvirtual.esap.edu.co') > -1){
            return location.origin + '/extension'
        }
        return location.origin;
    }
}